if has('win32') || has('win64')
	set fileencoding=cp932
endif

compiler php

setlocal makeprg=php\ -l\ %
setlocal errorformat=%m\ in\ %f\ on\ line\ %l

nnoremap <silent> <Leader>l :<C-u>make<CR>
nnoremap <silent> <Space>ref  :<C-u>Unite ref/phpmanual<CR>

" http://vim.g.hatena.ne.jp/ka-nacht/20090715/1247620578
"nnoremap <buffer> <Leader>l  :<C-u>execute '!' &l:filetype '-l' shellescape(expand('%'))<Return>
"autocmd BufWritePost * silent make %

inoremap <C-d> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-d> :call PhpDocSingle()<CR>
vnoremap <C-d> :call PhpDocRange()<CR> 
nnoremap <silent> <Leader>t :<C-u>!phpunit %<CR>

nnoremap <silent> <Space>ur :<C-u>Unite ref/phpmanual<CR>

" augroup phpsyntaxcheck
	" autocmd!
	" autocmd BufWrite *.php w !php  -l
" augroup END
