" -c jsファイルとして出力
" -p 出力結果を標準出力にプリント
" -b セーフティー関数で囲わない
nnoremap <Leader>p :<C-u>QuickRun -exec 'coffee -bp %s'<CR>
nnoremap <Leader>c :<C-u>QuickRun -exec 'coffee -cb %s'<CR>
nnoremap <Leader>l :<C-u>QuickRun -exec 'coffee -lint %s'<CR>

