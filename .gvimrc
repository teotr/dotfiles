scriptencoding utf-8
colorscheme jellybeans

set number            " 行番号表示
set go-=m go-=T       " ツールバー、メニューを非表示
set go-=r go-=R       " スクロールバーを非表示
set go-=l go-=L go-=b " スクロールバーを非表示
set go-=a

" set autochdir
set noerrorbells      " エラー時の音の抑制
set novisualbell      " エラー時のビジュアルベルの抑制
set visualbell t_vb=
set cmdheight=1       " コマンドラインの高さ (gvimはgvimrcで指定)
set cursorcolumn      " カーソル位置縦ライン表示
set clipboard=unnamed " クリップボードをWindowsと連携
set laststatus=2      " ステータスラインを常に表示
set linespace=4       " 行間の幅のピクセル数
set scrolloff=1000000 " スクロール時の余白確保
set showtabline=2     " タブを常に表示

" 補完候補の色づけ for vim7
" hi Pmenu ctermbg=white ctermfg=darkgray
" hi PmenuSel ctermbg=blue ctermfg=white
" hi PmenuSbar ctermbg=0 ctermfg=9


" ポップアップメニューのカラーを設定
" hi Pmenu guibg=#666666
" hi PmenuSel guibg=#8cd0d3 guifg=#666666
" hi PmenuSbar guibg=#333333
" xoria256のカラー設定で見づらいところを変更する
" ポップアップメニュー
" ポップアップメニューのアイテムの色
hi Pmenu          guifg=#f6f3e8     guibg=#444444     gui=NONE      ctermfg=NONE        ctermbg=NONE        cterm=NONE
" ポップアップメニューの現在選択しているアイテムの色
hi PmenuSel       guifg=#000000     guibg=#cae682     gui=NONE      ctermfg=NONE        ctermbg=NONE        cterm=NONE
" ポップアップメニューのスクロールバーの色
hi PmenuSbar      guifg=black       guibg=white       gui=NONE      ctermfg=black       ctermbg=white       cterm=NONE

" 全角スペースの表示
" highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=#666666
" au BufNewFile,BufRead * match ZenkakuSpace /　/

" 全角スペースとTODO/MEMOタスクを視覚化
" augroup MyHighlight
  " autocmd!
  " autocmd Colorscheme * highlight ZenkakuSpace ctermbg=DarkMagenta guibg=DarkMagenta
  " autocmd Colorscheme * highlight Note ctermfg=221 guifg=#FF9922 gui=underline
  " autocmd VimEnter,WinEnter * call matchadd('ZenkakuSpace', '\%u3000', -1)
  " autocmd VimEnter,WinEnter * call matchadd('Todo', 'TODO:', -1)
  " autocmd VimEnter,WinEnter * call matchadd('Note', 'NOTE:', -1)
 " autocmd VimEnter,WinEnter * call matchadd('ErrorMsg', '\%>120v.\+', -1)
" augroup END

" 全角スペースの表示
highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=#666666
if has('win32') && !has('gui_running')
	au BufNewFile,BufRead * match ZenkakuSpace /\%u8140/
else
	au BufNewFile,BufRead * match ZenkakuSpace /　/
endif

if has('gui_macvim')
	nnoremap <Space>fu :<C-u>setlocal fu! fu?<CR>
	set antialias
	set columns=90                " 幅
	set lines=60                  " 行数
	set fuoptions=maxvert,maxhorz
	set imdisableactivate         " 自動的に日本語入力(IM)をオンにする機能のみ禁止
	" set imdisable                 " IMを無効化
	"フォントの設定 スペースが含まれる場合は直前にバックスラッシュ
	" Ricty
	" set guifont=Ricty-Regular:h15
	set guifontwide=Ricty-Regular:h15
	set guifont=Consolas:h15
	"http://subtech.g.hatena.ne.jp/h2u/20110503/1304430186
	" set guifont=Andale_Mono:h16
	" set guifontwide=TakaoExGothic:h12

	" set guifont=Menlo_bold:h13
	" set guifontwide=TakaoExGothic:h12
	" set guifont=MigMix_2m_bold:h13
	" set guifont=DroidSansMonoSlashed:h12
	" set guifont=Inconsolata:h14
	" set guifontwide=meiryoKeConsole

	" Vim-users.jp - Hack #234: Vim外にいるときはVimを透けさせる
	" http://vim-users.jp/2011/10/hack234/
	augroup hack234
		autocmd!
		autocmd FocusGained * set transparency=10
		autocmd FocusLost * set transparency=50
	augroup END
elseif has('win32') || has('win64')
	set encoding=cp932
	set guifont=ProggyCleanTTSZBP:h12
	nnoremap <Space>fu :call ToggleFullScreen()<CR>
	function! ToggleFullScreen()
		if &guioptions =~# 'C'
			set guioptions-=C
			if exists('s:go_temp')
				if s:go_temp =~# 'm'
					set guioptions+=m
				endif
				if s:go_temp =~# 'T'
					set guioptions+=T
				endif
			endif
			simalt ~r
		else
			let s:go_temp = &guioptions
			set guioptions+=C
			simalt ~x
		endif
	endfunction
else 
	set guifont=MigMix\ 2M\ 10
	set columns=90
	set lines=45
endif
